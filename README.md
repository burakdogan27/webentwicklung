# README

# 1.Richtige Rails version und starten des Servers

 a) gem install rails -v 5.1.4  -> Gems installieren
 
 b) rails _5.1.4_ new abgabe2 
 -> Projekt anlegen
 
 c) gem list 
 -> Um sicherzugehen welche Versionenen man für die verschiedenen Gems hat 
 
 d) Symbol "~>" aus dem Gemfile 
 -> In der Rails version entfernen sodass nur noch 5.1.4 steht
 
 e)bundle update
 -> Sorgt dafür dass die Gems,die keine Versionen im Gemfile besitzen, auf die aktuellste Version gesetzt werden. 
 Mithilfe des Symbols "~>". Dadurch dass dieses Symbol entfernt wurde ist es möglich,die in der Gemfile spezifizierte Version zu nutzen.
 
 f)bundle install 
 -> installiert alle gems die man in dem Gemfile spezifiziert hat 
 
 g)rails server oder rails s
 -> um den Server zu starten
 
 
 
 
# A) Startseite modifizieren 

1) Controller generieren/anlegen 
>rails generate controller welcome index

-> Hierbei wird in dem Ordner welcome eine html.erb Datei namens "index" angelegt.Der Ordner welcome widerrum ist im Pfad "/abgabe2/app/views" zu finden.Diese html-Datei nutzen wir im nächsten Schritt um das layout der Startseite zu verändern.In dem wir die HTML-Datei mit HTML-Code versehen.


# B)Routing


1) Öffnen die routes.rb datei aus dem Pfad "/abgabe2/config"

Hier steht bereits folgendes dadurch,dass wir einen Controller generiert haben 

> get 'welcome/index' 

und gibt somit die Darstellung für den Nutzer im Webbrowser für den Pfad 
http://localhost:3000/welcome/index an

2) Angeben der Route 

um die Route für die Startseite anzugeben muss folgendes codiert werden : 

>root 'welcome#index'

Dadurch legen wir die html-Datei "index" als Startseite für unsere Ruby on Rails Applikation fest.
Die durch den Controller generierte Zeile kann entfernt werden.
Es sei denn man will neben der Startseite weiterhin über die Route "/welcome/index" auf die index Seite gelangen.

3) HTML-Inhalt
Zusätzlich ändern wir den Inhalt der index.html.erb Datei im Pfad "/app/views/welcome"

> Hello World! (Russisch) : Привет, мир!

Dieser Inhalt definiert nun die Ausgabe(Inhalt) der Datei "index.html.erb"



# C)Goodbye auf der Goodbye Route


1).Öffnen den welcomeController den wir zuvor definiert haben unter "/app/controllers" um dann die Datei welcome_controller.rb zu verändern.

2.)Innerhalb dieser Datei ist bereits die Action für die Html-Datei "index.html.erb" definiert 

3.)Um nun eine Action für die goodbye-Route festzulegen definieren wir zunächste eine neue Action innerhalb der welcomeController.rb - Datei.
Hierbei legen wir als Alternative den HTML-Text nicht in einer extra goodbye.html Datei,
sondern direkt in die neudefinierte Action mit rein.
Dies geschieht dadurch,dass wir render html: in den code mit schreiben und dann den auszugebenden Text in " " definieren.

>def goodbye render 

>html: "Auf Wiedersehen(Spanisch) : Adiós!"   

>end
    
4.)Zusätzlich müssen wir auch hier die route angeben in der routes.rb - Datei 

>get "goodbye", to: "welcome#goodbye"

In dem Fall sagen wir dass über die Route "welcome#goodbye" die im WelcomeController definierte Action "goodbye" aufgerufen werden soll.
Hier wird der dort gerenderte html-Text ausgegeben "Adiós!" ausgegeben.